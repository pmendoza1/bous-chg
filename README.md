# Test Project for Bous Challenge

## Getting started

To run the porject install Docker and run the following commands:

```
docker-compose build
docker-compose up -d
```

## Availible Endpoints

### [POST]: {{baseUrl}}/transaction/uploadDataFile
Loads the data from an excel file into the database
Body: form-data
file: excelFile.xlsx
Response:

```
{
    "debt": 499551000,
    "total_rows": 1999,
    "valid_rows": 1000
}
```

### [GET]:{{baseUrl}}/transaction?limit=5&page=0
Gets all the transactions with pagination
Response:
```
[
    {
        "debt": 499551000.0,
        "debt_by_city": "{'León': '10.791', ...}",
        "debt_by_company": "{'Xylekone Sa': '0.774', ...}",
        "filename": "excel_ejemplo_desarrolador_backend.xlsx",
        "id": 1,
        "load_date": "2023-07-03T23:48:38.222638",
        "total_rows": 1999,
        "valid_rows": 1000
    }
]
```

### [GET]:{{baseUrl}}/transaction/{transactionId}
Gets a transaction by Id
Response:
```
{
    "debt": 499551000.0,
    "debt_by_city": "{'León': '10.791', ...}",
    "debt_by_company": "{'Xylekone Sa': '0.774',...}",
    "total_rows": 1999,
    "valid_rows": 1000
}
```

### [GET]:{{baseUrl}}/transaction/last
Gets the latest transaction by upload date
Response:
```
{
    "debt": 499551000.0,
    "debt_by_city": "{'León': '10.791', ...}",
    "debt_by_company": "{'Xylekone Sa': '0.774', ...}",
    "filename": "excel_ejemplo_desarrolador_backend.xlsx",
    "id": 1,
    "load_date": "2023-07-03T23:48:38.222638",
    "total_rows": 1999,
    "valid_rows": 1000
}
```