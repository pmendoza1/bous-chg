from flask import Flask
from werkzeug.exceptions import HTTPException
from errors.json_error import handle_exception
from services.transaction.methods import service as transaction_service

from services.transaction.model import Base, Transaction
from sqlalchemy import create_engine

app = Flask(__name__)
app.register_error_handler(HTTPException, handle_exception)
app.register_blueprint(transaction_service, url_prefix='/transaction')
