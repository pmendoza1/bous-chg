from services.transaction.model import Base
from sqlalchemy import create_engine
from os import environ
from sqlalchemy.orm import sessionmaker

engine = create_engine(environ.get('SQLALCHEMY_DATABASE_URI'), echo=True)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)