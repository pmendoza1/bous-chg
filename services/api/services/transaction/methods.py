from flask import Blueprint, request, jsonify, abort, make_response
from errors.json_error import handle_exception
from .validations import excel_validations
from .model import Transaction, TransactionSchema
from sqlalchemy import desc
from shared.database import Session
import datetime
import openpyxl
import re
import numbers

service = Blueprint('transaction', __name__)
session = Session()
transaction_schema = TransactionSchema()

def _cells_to_values(cells):
    return list(map(lambda cell: cell.value, cells))

def _validate_row_data(data):
    if None in data:
        return False
    
    if (not isinstance(data[0], str) or re.search("^C-[\d]+", data[1]) == None or not isinstance(data[2], datetime.date) or not isinstance(data[3], str) 
        or not isinstance(data[4], str) or not isinstance(data[5], numbers.Number)):
        return False
    
    return True

def _partials_to_percent(partials, total):
    return {key: '%.3f'%(value * 100 / total) for key , value in partials.items()}
    
@service.route("/")
def get_transactions():
    page = request.args.get("page") or 1
    limit = request.args.get("limit") or 5
    print(page,limit)
    all_transactions = session.query(Transaction).limit(limit).offset(int(page)*int(limit))
    return jsonify([transaction_schema.dump(transaction) for transaction in all_transactions])

@service.route("/<int:id>")
def get_transaction_by_id(id):
    transaction = session.query(Transaction.total_rows, Transaction.valid_rows, Transaction.debt, Transaction.debt_by_city, Transaction.debt_by_company).filter_by(id=id).first()
    if transaction == None:
        abort(404, description=f'Transaction with id {id} not found')
    
    return jsonify(transaction_schema.dump(transaction))

@service.route("/last")
def last_transaction():
    transaction = session.query(Transaction).order_by(desc(Transaction.load_date)).first()    
    return jsonify(transaction_schema.dump(transaction))

@service.route("/uploadDataFile", methods=['POST'])
@excel_validations
def load_transaction_file():
    
    EXPECTED_HEADERS = ['Cliente', '# Contrato', 'Fecha de Compra', 'Ciudad', 'Empresa', 'Valor adeudado']
    
    file = request.files['file']        
    workBook = openpyxl.load_workbook(file, data_only=True)
    sheet = workBook.active
    headers = sheet['B3': 'G3'][0]
    col_names = _cells_to_values(headers)
    
    if not col_names == EXPECTED_HEADERS:
        abort(400, description="Use the original template file and don't alter any of the headers")
        
    total_rows = sheet.max_row - 5
    valid_rows = 0
    debt = 0
    debt_by_city = {}
    debt_by_company = {}
    load_date = datetime.datetime.today()
    filename = file.filename
    
    for row in range(5, sheet.max_row):
        row_data = sheet['B'+str(row): 'G'+str(row)][0]
        row_values = _cells_to_values(row_data)
        if _validate_row_data(row_values):
            valid_rows += 1
            debt += row_values[5]
            debt_by_city[row_values[3]] = row_values[5] + debt_by_city.get(row_values[3], 0)
            debt_by_company[row_values[4]] = row_values[5] + debt_by_company.get(row_values[4], 0)
            
    saved_equial_record = session.query(Transaction).filter_by(total_rows = total_rows, valid_rows = valid_rows, debt = debt).first()

    if not saved_equial_record == None:
        abort(400, description="You already loaded a file with the same data");
            
    percent_by_city = _partials_to_percent(debt_by_city, debt)
    percent_by_company = _partials_to_percent(debt_by_company, debt)
    new_transaction = Transaction(
        total_rows = total_rows,
        valid_rows = valid_rows,
        debt = debt,
        debt_by_city = percent_by_city,
        debt_by_company = percent_by_company,
        load_date = load_date,
        filename = filename
    )
    
    session.add(new_transaction)
    session.commit()
    
    return jsonify(total_rows = total_rows, valid_rows = valid_rows, debt = debt)