from functools import wraps
from flask import request, abort

ALLOWED_EXTENSIONS = {'xlsx', 'xlsm', 'xltm'}

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def excel_validations(f):
    @wraps(f)
    def validate_file(*args, **kwargs):
        
        if 'file' not in request.files:
            abort(400, description="Send the excel file using the key named file")
        
        file = request.files['file']
            
        if file.filename == '':
            abort(400, description="You have to select a file")
        
        if file and not allowed_file(file.filename):
            abort(400, description="Only excel files are allowed")
            
        return f(*args, **kwargs)
    
    return validate_file