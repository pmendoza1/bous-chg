from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, PickleType, DateTime, String
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

Base = declarative_base()

class Transaction(Base):
    __tablename__ = "transaction"
    id = Column(Integer, primary_key=True)
    total_rows = Column(Integer, nullable=False)
    valid_rows = Column(Integer, nullable=False)
    debt = Column(Float, nullable=False)
    debt_by_city = Column(PickleType, nullable=False)
    debt_by_company = Column(PickleType, nullable=False)
    load_date = Column(DateTime, nullable=False)
    filename = Column(String, nullable=False)
    
class TransactionSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Transaction
        include_relationships = True
        load_instance = True